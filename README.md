### Automation Script New VIP Services for Citrix Netscaler ###

Takes in a list of arguments like so:  python3 netscaler_service_add_dts.py server01 HelloWorld 8080 /healthcheck.html 200 HTTP

Interacts with the Netscaler Nitro API to create the server object, the monitor, and the service for the app in question "HelloWorld."

Intended to be used in automating instance creation in a multi-tenant JBOSS/Tomcat environment.
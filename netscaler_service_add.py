#import the necessary libraries and classes
import requests
import sys
import socket
import json
from Monitor_Add import *
from Server_Add import *
from Service_Add import *

#set some API vars
url = 'http://10.134.10.10/nitro/v1/config/'
headers = {'Content-type': 'application/json','X-NITRO-USER': 'apiadd', 'X-NITRO-PASS': 'REDACTED'}

#get arument list using sys module
sys.argv

#set vars based on args
servername = str(sys.argv[1])
appname = str(sys.argv[2])
port = str(sys.argv[3])
healthcheck = str(sys.argv[4])
stringresponse = str(sys.argv[5])
servicetype = str(sys.argv[6])
serverarray = servername.split('.')
servershortname = serverarray[0]
monitorname = appname+"-"+servershortname+"-"+servicetype

# print "I have parsed these params:"
# print "servername = "+servername
# print "appname = "+appname
# print "port = "+port
# print "healthcheck = "+healthcheck
# print "response = "+stringresponse
# print "servicetype = "+servicetype

#check to see if server exists, if not, add it or fail trying
Server_Add(url,headers,servershortname)

#check to see if monitor exists, if not, add it or fail trying
Monitor_Add(url,headers,servicetype,stringresponse,monitorname,healthcheck)

#check to see if service exists, if not, add it or fail trying
Service_Add(url,headers,servicetype,servername,monitorname,port)

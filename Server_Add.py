import sys
import requests
import json
import socket

class Server_Add(object):
    """docstring for Server_Add"""
    def __init__(self, arg, arg1, arg2):
        super(Server_Add, self).__init__()
        url = arg
        headers = arg1
        servershortname = arg2
        serverrequesturl = url+"server/"+servershortname
        serverexistsresponse = requests.get(serverrequesturl, headers=headers)
        try:
            if serverexistsresponse.status_code != 200:
                dnsip = socket.gethostbyname(servershortname)
                payload = {'server':{'name':servershortname,'ipaddress':dnsip}}
                posturl = url+"server?action=add"
                serveradd = requests.post(posturl, data=json.dumps(payload), headers=headers)
                if serveradd.status_code != 201:
                    print("Something went wrong with adding the server.")
                    sys.exit(1)
        except Exception as e:
            raise e
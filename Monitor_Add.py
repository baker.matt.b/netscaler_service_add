import sys
import requests
import json
import socket

class Monitor_Add(object):
    """docstring for Monitor_Add"""
    def __init__(self, arg, arg1, arg2, arg3, arg4, arg5):
        super(Monitor_Add, self).__init__()
        url = arg
        headers = arg1
        servicetype = arg2
        stringresponse = arg3
        monitorname = arg4
        healthcheck = arg5
        posturl = url+"lbmonitor?action=add"
        monitorrequesturl = url+"lbmonitor/"+monitorname
        monitorresponse = requests.get(monitorrequesturl, headers=headers)
        try:
            if monitorresponse.status_code != 200:
                if ( servicetype == "HTTP" and stringresponse == "200" ):
                    monitorpayload= {'lbmonitor':{'monitorname':monitorname,'type':'HTTP','httprequest':'GET '+healthcheck,'respcode':stringresponse}}
                    monitoradd = requests.post(posturl, data=json.dumps(monitorpayload), headers=headers)
                    if monitoradd.status_code != 201:
                        print("Something went wrong with adding the monitor.")
                        sys.exit(1)
                elif ( servicetype == "HTTPS" and stringresponse == "200" ):
                    monitorpayload= {'lbmonitor':{'monitorname':monitorname,'type':'HTTP','httprequest':'GET '+healthcheck,'respcode':stringresponse,'secure':'yes'}}
                    monitoradd = requests.post(posturl, data=json.dumps(monitorpayload), headers=headers)
                    if monitoradd.status_code != 201:
                        print("Something went wrong with adding the monitor.")
                        sys.exit(1)
                elif ( servicetype == "HTTP"):
                    monitorpayload= {'lbmonitor':{'monitorname':monitorname,'type':'HTTP-ECV','send':'GET '+healthcheck,'recv':stringresponse}}
                    monitoradd = requests.post(posturl, data=json.dumps(monitorpayload), headers=headers)
                    if monitoradd.status_code != 201:
                        print("Something went wrong with adding the monitor.")
                        sys.exit(1)
                elif ( servicetype == "HTTPS"):
                    monitorpayload= {'lbmonitor':{'monitorname':monitorname,'type':'HTTP-ECV','send':'GET '+healthcheck,'recv':stringresponse,'secure':'yes'}}
                    monitoradd = requests.post(posturl, data=json.dumps(monitorpayload), headers=headers)
                    if monitoradd.status_code != 201:
                        print("Something went wrong with adding the monitor.")
                        sys.exit(1)
                else:
                    print("I'm not sure what you're asking of me.  Contact the UNIX team for more info.")
        except Exception as e:
            raise e
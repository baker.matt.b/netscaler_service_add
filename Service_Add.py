import sys
import requests
import json
import socket

class Service_Add(object):
    """docstring for Service_Add"""
    def __init__(self, arg, arg1, arg2, arg3, arg4, arg5):
        super(Service_Add, self).__init__()
        url = arg
        headers = arg1
        servicetype = arg2
        servername = arg3
        monitorname = arg4
        port = arg5
        posturl = url+"service?action=add"
        bindposturl = url+"lbmonitor_service_binding?action=add"
        saveurl = url+"nsconfig?action=save"
        servicerequesturl = url+"service/"+monitorname
        serviceresponse = requests.get(servicerequesturl, headers=headers)
        try:
            if serviceresponse.status_code != 200:
                servicepayload={'service':{'name':monitorname,'servername':servername,'servicetype':servicetype,'port':port,'healthmonitor': 'YES','cip':'ENABLED','cipheader':'client-ip'}}
                bindingpayload={'lbmonitor_service_binding':{'monitorname':monitorname,'servicename':monitorname}}
                serviceadd = requests.post(posturl, data=json.dumps(servicepayload), headers=headers)
                if serviceadd.status_code != 201:
                    print("Something went wrong with adding the monitor.")
                    sys.exit(1)
                bindservice = requests.post(bindposturl, data=json.dumps(bindingpayload), headers=headers)
                if bindservice.status_code != 201:
                    print("Something went wrong with binding the monitor to the service.")
                    sys.exit(1)                 
            savepayload={ "nsconfig": {} }
            saveconfig = requests.post(saveurl, data=json.dumps(savepayload), headers=headers)
            if saveconfig.status_code != 200:
                print("Something went wrong with saving the config.")
                sys.exit(1)
        except Exception as e:
            raise e